import Vue from 'vue';
import VueRouter from 'vue-router';
// eslint-disable-next-line import/extensions
import Home from '../views/select-subject-grade';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/select-mode',
    name: 'Mode',
    component: () => import('../views/select-mode'),
  },
  {
    path: '/conditional',
    name: 'Conditional',
    component: () => import('../views/conditional'),
  },
  {
    path: '/browsingtable',
    name: 'Browsingtable',
    component: () => import('../views/browsing-table'),
  },
  {
    path: '/edittable',
    name: 'Edittable',
    component: () => import('../views/edit-table'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
