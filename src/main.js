import Vue from 'vue';
import './plugins/fontawesome';
import './plugins/bootstrap-vue';
import VueGoodTablePlugin from 'vue-good-table';
import App from './App.vue';
import router from './router';

// import the styles
import 'vue-good-table/dist/vue-good-table.css';

Vue.use(VueGoodTablePlugin);

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
